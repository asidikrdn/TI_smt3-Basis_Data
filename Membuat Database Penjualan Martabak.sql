/*
DDL

Create	: Membuat, dapat digunakan untuk membuat database maupun tabel
Drop	: Menghapus, digunakan untuk menghapus database atau tabel
Use		: Menggunakan database

Note
- Jangan hapus database master
- Database yg sedang digunakan tidak dapat di drop
*/

/* Membuat Database */
CREATE DATABASE ASRDB_MARTABAK

/* Menggunakan Database */
USE ASRDB_MARTABAK

/* Membuat Tabel v1 (tanpa primary key) */
CREATE TABLE tblJenisMartabak
(
	id_jenis Varchar(2),
	NamaJenis Varchar(10)
)


/* Membuat Tabel v1 (dengan primary key) */
CREATE TABLE tblJenisMartabak
(
	id_jenis Varchar(2),
	NamaJenis Varchar(10),
	constraint PKJenisMartabak Primary Key (id_jenis)
)

/*
DML => Data Manipulation

Insert	: Menambahkan/Memasukkan nilai ke dalam tabel, mengisinya harus urut dari atribut pertama sampai terakhir, dipisahkan oleh tanda koma
Select	: Membaca/Menampilkan nilai pada suatu tabel. Menggunakan simbol * untuk menampilkan seluruhnya, atau menyebutkan nama atributnya jika ingin menampilkan atribut tertentu

*/

/* Menambahkan Value/Nilai pada tabel tblJenisMartabak */
INSERT INTO tblJenisMartabak values ('01', 'Manis')
INSERT INTO tblJenisMartabak values ('02', 'Asin')

/* Menampilkan nilai seluruh atribut pada tabel tblJenisMartabak */
select * from tblJenisMartabak

/* Menampilkan nilai atribut NamaJenis pada tabel tblJenisMartabak */
select NamaJenis from tblJenisMartabak


/* Menghapus tabel tblJenisMartabak */
drop table tblJenisMartabak


/* Membuat Tabel tblMenuMartabak (dengan foreign key) */
CREATE TABLE tblMenuMartabak
(
	id_jenis varchar(2) Foreign Key References tblJenisMartabak (id_jenis),
	id_menu Varchar(3),
	NamaMenu Varchar(50),
	Harga Int,
	constraint PKMenuMartabak Primary Key (id_menu)
)

drop table tblPenjualan
drop table tblPelanggan
drop table tblMenuMartabak

/* Membuat Tabel tblPelanggan (dengan foreign key) */
CREATE TABLE tblPelanggan
(
	id_pelanggan Varchar(5),
	NamaPelanggan Varchar(20),
	Alamat Varchar(30),
	Telp Varchar(15),
	constraint PKPelanggan Primary Key (id_pelanggan)
)

/* Membuat Tabel tblPenjualan (dengan foreign key) */
CREATE TABLE tblPenjualan
(
	id_menu Varchar(3) foreign key references tblMenuMartabak (id_menu), 
	id_pelanggan Varchar(5) foreign key references tblPelanggan (id_pelanggan),
	tanggal date,
	jumlah_beli int,
	id_penjualan Varchar(5)
)
