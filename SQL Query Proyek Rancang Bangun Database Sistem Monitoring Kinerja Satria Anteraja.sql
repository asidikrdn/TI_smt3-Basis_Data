-- Membuat Database dengan nama DB_SatriaPeformance --
CREATE DATABASE DB_SatriaPeformance

-- Menggunakan Database DB_SatriaPeformance --
USE DB_SatriaPeformance

-- Membuat tabel zona --
Create Table tbl_Zona
(
	id_zona Varchar(5),
	area Varchar(50),
	constraint PKZona primary key (id_zona)
)

-- Membuat tabel satria --
Create Table tbl_Satria
(
	nip	int,
	nama varchar(25),
	id_zona varchar(5) foreign key references tbl_Zona (id_zona),
	constraint PKSatria primary key (nip)
)

-- Membuat table keterangan --
Create Table tbl_Keterangan
(
	id_ket char,
	keterangan varchar(16),
	constraint PKKeterangan primary key (id_ket)
)

-- Membuat table peformance --
Create Table tbl_Peformance
(
	tanggal date,
	nip int foreign key references tbl_Satria (nip),
	delivery int,
	pend_delivery int,
	pickup int,
	pend_pickup int,
	id_ket char foreign key references tbl_Keterangan (id_ket)
)





/* Mengisi Tabel Zona */
INSERT INTO tbl_Zona VALUES ('TJR00', 'Bantar Kemang, Katulampa, Vila Duta')
INSERT INTO tbl_Zona VALUES ('CBA00', 'Gunung Geulis, Griya Soka, Pangulaan, Pasir Maung')
INSERT INTO tbl_Zona VALUES ('BOJ00', 'Bantar Peuteuy, Tanuwijaya, Sukajaya')

SELECT * FROM tbl_Zona
DELETE FROM tbl_Zona


/* Mengisi Tabel Keterangan */
INSERT INTO tbl_Keterangan VALUES ('H', 'Hadir')
INSERT INTO tbl_Keterangan VALUES ('L', 'Libur')
INSERT INTO tbl_Keterangan VALUES ('S', 'Sakit')
INSERT INTO tbl_Keterangan VALUES ('I', 'Izin')
INSERT INTO tbl_Keterangan VALUES ('A', 'Tanpa Keterangan')

SELECT * FROM tbl_Keterangan
DELETE FROM tbl_Keterangan


/* Mengisi Tabel Satria */
INSERT INTO tbl_Satria VALUES ('10004376', 'Bambang Hermawan', 'TJR00')
INSERT INTO tbl_Satria VALUES ('10031747', 'Angga Taufik', 'TJR00')
INSERT INTO tbl_Satria VALUES ('10017669', 'Dirman', 'TJR00')
INSERT INTO tbl_Satria VALUES ('10028618', 'M. Arif Hidayatulloh', 'TJR00')
INSERT INTO tbl_Satria VALUES ('10025654', 'Ahmad Sidik Rudini', 'BOJ00')
INSERT INTO tbl_Satria VALUES ('10044412', 'Ikhsan Ramadhan P', 'BOJ00')
INSERT INTO tbl_Satria VALUES ('10014702', 'Adam Aditya R', 'CBA00')
INSERT INTO tbl_Satria VALUES ('10042569', 'Ibnu Haryawan', 'CBA00')
INSERT INTO tbl_Satria VALUES ('10022923', 'Andreas Liviana', 'TJR00')
INSERT INTO tbl_Satria VALUES ('10013344', 'Muhammad Syahril', 'TJR00')

SELECT * FROM tbl_Satria
Delete from tbl_Satria where nip=10044412


/* Mengisi Tabel Peformance */
	/* 1-7 Okt */
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10004376', '5', '0', '92', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10013344', '4', '0', '84', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10017669', '0', '0', '100', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10022923', '67', '0', '2', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10025654', '0', '0', '72', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10028618', '5', '0', '75', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-1', '10031747', '0', '0', '0', '0', 'L')

	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10004376', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10013344', '8', '0', '69', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10017669', '49', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10022923', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10025654', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10028618', '60', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-2', '10031747', '0', '0', '92', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10004376', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10013344', '5', '0', '22', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10017669', '0', '0', '57', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10022923', '36', '0', '17', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10025654', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10028618', '55', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-3', '10031747', '0', '0', '55', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10004376', '4', '0', '148', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10013344', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10017669', '0', '0', '140', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10022923', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10025654', '49', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10028618', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-4', '10031747', '0', '0', '0', '0', 'L')

	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10004376', '9', '0', '104', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10013344', '6', '0', '55', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10017669', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10022923', '46', '0', '12', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10025654', '57', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10028618', '59', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-5', '10031747', '0', '0', '101', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10004376', '1', '0', '106', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10013344', '15', '0', '91', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10017669', '0', '0', '95', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10022923', '14', '0', '10', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10025654', '66', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10028618', '70', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-6', '10031747', '0', '0', '0', '0', 'L')

	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10004376', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10013344', '20', '0', '56', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10017669', '0', '0', '80', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10022923', '74', '0', '2', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10025654', '75', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10028618', '81', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-10-7', '10031747', '0', '0', '81', '0', 'H')

	/* 21-27 Nov */
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10004376', '0', '0', '51', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10013344', '22', '0', '31', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10017669', '25', '0', '33', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10022923', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10025654', '21', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10028618', '43', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10031747', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10042569', '48', '0', '12', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-21', '10014702', '61', '0', '4', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10004376', '6', '0', '45', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10013344', '9', '0', '86', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10017669', '9', '0', '53', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10022923', '81', '0', '9', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10025654', '10', '0', '10', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10028618', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10031747', '4', '0', '60', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10042569', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-22', '10014702', '0', '0', '0', '0', 'L')

	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10004376', '0', '0', '72', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10013344', '5', '0', '56', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10017669', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10022923', '102', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10025654', '13', '0', '1', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10028618', '67', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10031747', '0', '0', '73', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10042569', '60', '0', '11', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-23', '10014702', '65', '0', '36', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10004376', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10013344', '5', '0', '46', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10017669', '38', '0', '59', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10022923', '64', '0', '12', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10025654', '11', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10028618', '73', '0', '2', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10031747', '0', '0', '84', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10042569', '49', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-24', '10014702', '30', '0', '16', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10004376', '47', '0', '58', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10013344', '0', '0', '62', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10017669', '30', '0', '71', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10022923', '40', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10025654', '12', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10028618', '73', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10031747', '28', '0', '87', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10042569', '36', '0', '8', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-25', '10014702', '36', '0', '19', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10004376', '24', '0', '57', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10013344', '13', '0', '73', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10017669', '29', '0', '122', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10022923', '48', '0', '1', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10025654', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10028618', '96', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10031747', '13', '0', '112', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10042569', '58', '0', '16', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-26', '10014702', '49', '0', '38', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10004376', '63', '0', '80', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10013344', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10017669', '38', '0', '73', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10022923', '57', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10025654', '0', '0', '7', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10028618', '106', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10031747', '0', '0', '114', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10042569', '61', '0', '11', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-11-27', '10014702', '107', '0', '26', '0', 'H')

	/* 8-15 Des */
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10004376', '47', '0', '81', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10013344', '34', '0', '63', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10017669', '29', '0', '104', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10022923', '67', '0', '2', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10025654', '55', '0', '13', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10028618', '135', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10031747', '17', '0', '104', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10042569', '73', '0', '6', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-8', '10014702', '94', '0', '24', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10004376', '0', '0', '128', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10013344', '38', '0', '75', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10017669', '0', '0', '123', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10022923', '112', '0', '4', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10025654', '56', '0', '4', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10028618', '113', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10031747', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10042569', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-9', '10014702', '88', '0', '13', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10004376', '23', '0', '81', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10013344', '53', '0', '81', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10017669', '16', '0', '86', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10022923', '102', '0', '5', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10025654', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10028618', '162', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10031747', '12', '0', '90', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10042569', '54', '0', '16', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-10', '10014702', '94', '0', '31', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10004376', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10013344', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10017669', '3', '0', '135', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10022923', '135', '0', '9', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10025654', '27', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10028618', '88', '0', '45', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10031747', '57', '0', '11', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10042569', '63', '0', '20', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-11', '10014702', '0', '0', '0', '0', 'L')

	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10004376', '0', '0', '122', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10013344', '59', '0', '94', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10017669', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10022923', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10025654', '18', '0', '20', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10028618', '0', '0', '0', '0', 'L')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10031747', '0', '0', '125', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10042569', '84', '0', '39', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-12', '10014702', '108', '0', '27', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10004376', '0', '0', '193', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10013344', '12', '0', '142', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10017669', '14', '0', '206', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10022923', '104', '0', '33', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10025654', '80', '0', '15', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10028618', '88', '0', '75', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10031747', '0', '0', '236', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10042569', '76', '0', '18', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-13', '10014702', '94', '0', '64', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10004376', '0', '0', '110', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10013344', '9', '0', '103', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10017669', '10', '0', '103', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10022923', '93', '0', '27', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10025654', '44', '0', '25', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10028618', '122', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10031747', '0', '0', '93', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10042569', '77', '0', '23', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-14', '10014702', '125', '0', '24', '0', 'H')

	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10004376', '0', '0', '113', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10013344', '12', '0', '90', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10017669', '83', '0', '71', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10022923', '100', '0', '6', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10025654', '66', '0', '19', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10028618', '142', '0', '0', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10031747', '1', '0', '94', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10042569', '103', '0', '9', '0', 'H')
	INSERT INTO tbl_Peformance VALUES ('2021-12-15', '10014702', '143', '0', '31', '0', 'H')



/* Menambahkan Kolom Point pada tabel keterangan */
Alter table tbl_keterangan add poin int

select * from tbl_keterangan

/* Mengisi kolom poin pada tabel keterangan */
update tbl_Keterangan set poin = 100
where tbl_keterangan.id_ket = 'H' or tbl_keterangan.id_ket = 'L'

update tbl_Keterangan set poin = 50
where tbl_keterangan.id_ket = 'S' or tbl_keterangan.id_ket = 'I'

update tbl_Keterangan set poin = 0
where tbl_keterangan.id_ket = 'A'

update tbl_Peformance set delivery = 0
where tbl_Peformance.delivery = NULL


Select * from tbl_Zona
Select * from tbl_Satria
Select * from tbl_Keterangan
Select * from tbl_Peformance

drop table tbl_Peformance
drop table tbl_Keterangan

/* Sebagian record pada tabel Peformance, diisi menggunakan metode import data dari Ms.Excel */

delete from tbl_Peformance
where nip is NULL

-- Menampilkan Data Peformance pada tgl 4 Okt 2021--
select tbl_Peformance.tanggal, tbl_Satria.nama, tbl_Peformance.delivery, tbl_Peformance.pickup, tbl_Keterangan.keterangan
from tbl_Peformance, tbl_Satria, tbl_Keterangan
where tbl_Peformance.nip = tbl_Satria.nip 
	AND tbl_Peformance.id_ket = tbl_Keterangan.id_ket
	AND tbl_Peformance.tanggal = '2021-10-04'

-- Menampilkan data peformance pada rentang tanggal 9 Des 2021 s/d 11 Des 2021 --
Select tbl_Peformance.tanggal, tbl_Satria.nama, tbl_Peformance.delivery, tbl_Peformance.pickup, tbl_Keterangan.keterangan, tbl_Keterangan.poin
from tbl_Satria, tbl_Peformance, tbl_Keterangan
Where tbl_Peformance.nip = tbl_Satria.nip and tbl_Peformance.id_ket = tbl_Keterangan.id_ket and tbl_Peformance.tanggal between '2021-12-9' and '2021-12-11'
Order By tbl_Peformance.tanggal ASC

-- Menampilkan Nama Satria yang nipnya adalah 10004376 --
Select tbl_Satria.nama, tbl_Zona.area
from tbl_Satria, tbl_Zona
where tbl_Satria.id_zona = tbl_Zona.id_zona and tbl_Satria.nip = 10004376

-- Menampilkan total delivery, total pickup, dan poin kehadiran dari satria dengan nip 10004376 dalam rentang waktu 1 Desember hingga 31 Desember 2021 --
Select SUM(tbl_Peformance.delivery) as Total_Delivery, SUM(tbl_Peformance.pickup) as Total_Pickup, sum(tbl_keterangan.poin) as Total_Poin_Kehadiran
from tbl_Peformance, tbl_Satria, tbl_Keterangan
where tbl_Peformance.nip = tbl_Satria.nip and 
	tbl_Peformance.id_ket = tbl_Keterangan.id_ket and tbl_Peformance.nip = 10004376 and 
	tbl_Peformance.tanggal between '2021-12-1' and '2021-12-31'

-- Menampilkan Jumlah Delivery dan Pickup terbanyak dari satria dengan nip 10004376 dalam rentang waktu 1 Desember hingga 31 Desember 2021 --
Select MAX(tbl_Peformance.delivery) as Delivery_Terbanyak, MAX(tbl_Peformance.pickup) as Pickup_Terbanyak
from tbl_Peformance, tbl_Satria
where tbl_Peformance.nip = tbl_Satria.nip and tbl_Peformance.nip = 10004376 and 
	tbl_Peformance.tanggal between '2021-12-1' and '2021-12-31'

