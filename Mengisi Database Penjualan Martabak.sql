/* Menambahkan Value/Nilai pada tabel tblJenisMartabak */
INSERT INTO tblJenisMartabak values ('01', 'Manis')
INSERT INTO tblJenisMartabak values ('02', 'Asin')


/* Mengisi Tabel Menu Martabak */
INSERT into tblMenuMartabak VALUES ('01', 'M01', 'Martabak Susu', '12000')
INSERT into tblMenuMartabak VALUES ('01', 'M02', 'Martabak Kacang Susu', '15000')
INSERT into tblMenuMartabak VALUES ('01', 'M03', 'Martabak Kacang Susu Coklat', '18000')
INSERT into tblMenuMartabak VALUES ('01', 'M04', 'Martabak Pisang Coklat', '18000')
INSERT into tblMenuMartabak VALUES ('01', 'M05', 'Martabak Keju Pisang Coklat', '20000')
INSERT into tblMenuMartabak VALUES ('01', 'M06', 'Martabak Keju Pisang Kacang Coklat', '25000')

INSERT into tblMenuMartabak VALUES ('02', 'A01', 'Martabak Telur Ayam', '15000')
INSERT into tblMenuMartabak VALUES ('02', 'A02', 'Martabak Telur Ayam Super', '18000')
INSERT into tblMenuMartabak VALUES ('02', 'A03', 'Martabak Telur Ayam Spesial', '20000')
INSERT into tblMenuMartabak VALUES ('02', 'A04', 'Martabak Telur Bebek', '18000')
INSERT into tblMenuMartabak VALUES ('02', 'A05', 'Martabak Telur Bebek Super', '25000')
INSERT into tblMenuMartabak VALUES ('02', 'A06', 'Martabak Telur Bebek Spesial', '30000')



/* Mengisi Tabel Pelanggan */
INSERT into tblPelanggan VALUES ('P01', 'Ahmad Sidik', 'Bogor', '087711356758')
INSERT into tblPelanggan VALUES ('P02', 'Muhammad Faiz', 'Depok', '085155279686')
INSERT into tblPelanggan VALUES ('P03', 'Qois Absyie', 'Cibinong', '081255658878')
 
Select * from tblPelanggan



/* Mengisi Tabel Penjualan */
INSERT INTO tblPenjualan VALUES ('A01', 'P01', '2022-1-1', '1', 'tr001')
INSERT INTO tblPenjualan VALUES ('A02', 'P01', '2022-1-1', '3', 'tr001')
INSERT INTO tblPenjualan VALUES ('M01', 'P01', '2022-1-1', '1', 'tr001')
INSERT INTO tblPenjualan VALUES ('A05', 'P02', '2022-1-2', '6', 'tr002')
INSERT INTO tblPenjualan VALUES ('M03', 'P02', '2022-1-2', '4', 'tr002')
INSERT INTO tblPenjualan VALUES ('A04', 'P03', '2022-1-3', '5', 'tr003')
INSERT INTO tblPenjualan VALUES ('M02', 'P03', '2022-1-3', '9', 'tr003')
INSERT INTO tblPenjualan VALUES ('A03', 'P02', '2022-1-4', '2', 'tr004')
INSERT INTO tblPenjualan VALUES ('M02', 'P02', '2022-1-11', '5', 'tr005')
INSERT INTO tblPenjualan VALUES ('M02', 'P01', '2022-1-11', '1', 'tr006')
INSERT INTO tblPenjualan VALUES ('M02', 'P02', '2022-1-11', '4', 'tr007')
delete from tblPenjualan


select * from tblPenjualan
select * from tblPelanggan
select * from tblMenuMartabak
select * from tblJenisMartabak











use ASRDB_MARTABAK

select * from tblMenuMartabak

DELETE from tblMenuMartabak