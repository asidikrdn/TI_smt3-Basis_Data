/* MULTITABLE */
/* Contoh */

/* Menampilkan semua pelanggan yang membeli martabak kacang susu */
/* Penulisan Satu Baris */
SELECT tblPelanggan.NamaPelanggan, tblPenjualan.tanggal, tblPenjualan.jumlah_beli, tblMenuMartabak.NamaMenu from tblPelanggan, tblPenjualan, tblMenuMartabak where tblPelanggan.id_pelanggan = tblPenjualan AND 

/* Penulisan Multi Baris */
SELECT tblPelanggan.NamaPelanggan, 
	tblPenjualan.tanggal, 
	tblPenjualan.jumlah_beli, 
	tblMenuMartabak.NamaMenu,
	tblMenuMartabak.Harga, tblPenjualan.jumlah_beli * tblMenuMartabak.Harga as [Total Bayar]
from tblPelanggan, 
	tblPenjualan, 
	tblMenuMartabak 
where tblPelanggan.id_pelanggan = tblPenjualan.id_pelanggan /* Untuk memastikan pelanggan yang ada di tbl penjualan, terdaftar pada tabel master pelanggan */
	AND tblMenuMartabak.id_menu = tblPenjualan.id_menu /* Untuk memastikan menu yang ada di tbl penjualan, terdaftar pada tabel master menu */
	AND tblPenjualan.id_menu = 'M02'


SELECT sum (tblPenjualan.jumlah_beli * tblMenuMartabak.Harga) as [Total Pendapatan]
from tblPelanggan, 
	tblPenjualan, 
	tblMenuMartabak 
where tblPelanggan.id_pelanggan = tblPenjualan.id_pelanggan /* Untuk memastikan pelanggan yang ada di tbl penjualan, terdaftar pada tabel master pelanggan */
	AND tblMenuMartabak.id_menu = tblPenjualan.id_menu /* Untuk memastikan menu yang ada di tbl penjualan, terdaftar pada tabel master menu */
	AND tblPenjualan.id_menu = 'M02'
